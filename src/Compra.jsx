import React, { useState } from "react";
import { Container, Button } from "reactstrap";
import styled from 'styled-components';
import { productes } from "./Fruita.jsx";

const FormatoCarrito = styled.div`
    height: 150px;
    width: 300px;
    background-color: orange;
    color: white;
    float:right-top;
    border-radius: 30px;
    text-align:center;
    float: right;
    margin-right: 310px;
    border:5px solid grey;
   

`;


const FormatoCeldas = styled.div`
    height: 150px;
    width: 300px;
    background-color: green;
    color: white;
    margin:5px;
    border-radius: 30px;
    border:5px solid grey;
    text-align:center;
    float: left;

`;

export default (props) => {
   
    let fruites = productes.filter(el=> el.units>0).map(el => (
        <FormatoCeldas>
            <h5>{el.nom} </h5>
            <h5>{el.preu} &euro; unitat</h5>
            <h5>{el.units}</h5>
            <Button onClick={()=>{props.funcionBorrar(el.id)}} id="boton-sumar" className="btn btn-success">Treu</Button>
        </FormatoCeldas>

    ));

    return (
        <>

            <FormatoCarrito ><h3>Carrito</h3>
                <ul id="carrito" className="list-group"></ul>
                <p>Total = {props.funcionTotal}<span id="total"></span>&euro;</p>

                <Button id="boton-vaciar" className="btn btn-danger">Buida</Button>
            </FormatoCarrito>
            {fruites}
        </>
    );
};