import React, { useState, useEffect } from "react";
import { Container, Row, Col, Button } from "reactstrap";
import Fruita from "./Fruita";
import Ticket from "./Ticket";
import Compra from "./Compra";

import "bootstrap/dist/css/bootstrap.min.css";

import styled from 'styled-components';
import { productes } from "./Fruita.jsx";

const FormatoCeldas = styled.div`
    height: 150px;
    width: 300px;
    background-color: green;
    color: white;
    margin:5px;
    border-radius: 30px;
    border:5px solid grey;
    text-align:center;
    float: left;

`;

export default () => {
  const fruites = productes;
  //const [llista, setLlista] = useState({ fruites });
  const [nuevaLista, setNuevaLista] = useState([]);
  const [guardaItems, setGuardaItems] = useState([]);

  const afegirItems = (id) => {
    setNuevaLista(fruites.map(
      el => {
        if (el.id === id) {
          el.units = el.units + 1;
          return (el);
        }
        return el;
      }
    ));
    console.log({ nuevaLista });
  };

  const borraItems = (id) => {
    setNuevaLista(fruites.map(
      el => {
        if (el.id === id) {
          el.units = el.units - 1;
          return (el);
        }
        return el;
      }
    ));
    console.log({ nuevaLista });
  };

  const Total = (id) => {
    setNuevaLista(fruites.map(
      el => {
        if (el.id === id) {
          el.units = el.units + el.units;
          return (el);
        }
        return el;
      }
    ));
    console.log({ nuevaLista });
  };


  return (
    <>
      <Ticket funcionAfegir={afegirItems} />
      <Compra funcionBorrar={borraItems} funcionTotal={Total}/>
    </>

  );

};