import React, { useState } from "react";
import { Container, Button } from "reactstrap";
import styled from 'styled-components';
import { productes } from "./Fruita.jsx";


const FormatoCeldas = styled.div`
    height: 150px;
    width: 300px;
    background-color: green;
    color: white;
    margin:5px;
    border-radius: 30px;
    border:5px solid grey;
    text-align:center;
    float: left;

`;
const FormatoCarrito = styled.div`
    height: 150px;
    width: 300px;
    background-color: orange;
    color: white;
    float:right-top;
    border-radius: 30px;
    text-align:center;
    float: right;
    margin-right: 310px;
    border:5px solid grey;
   

`;
const Titulo = styled.h2`
 
    text-align:center;
   

`;
export default (props) => {


    let fruites = productes.map(el => (

        <FormatoCeldas>
            <h5>{el.nom} </h5>
            <h5>{el.preu} &euro; unitat</h5>
            <Button onClick={()=>{props.funcionAfegir(el.id)}} id="boton-sumar" className="btn btn-success">Agafa</Button>
        </FormatoCeldas>

    ));



    return (
        <><Titulo>Carro de la compra</Titulo>
            <Container>
                {fruites}
            </Container>
        </>
    );
};
